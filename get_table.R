get_table <- function(table_id, project_id = "sportmaster-bq2018", dataset_id = 11442877, proxy_usage = F){
  source("~/R/rbigquery/proto_request.R")
  r_url <- paste("projects", project_id, "datasets", dataset_id, "tables", table_id,  sep = "/")
  #if(!identical(fields, F)) r_url <- paste0(r_url, "/?selectedFields=", fields)
  x <- proto_request(r_url, proxy_usage)
  x
}