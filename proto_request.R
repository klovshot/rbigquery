proto_request <- function(rurl, proxy_usage = F) {
  
  require(httr)
  require(jsonlite)
  source("https://bitbucket.org/klovshot/roauth/raw/master/get_cached_token.R")
  
  rurl <- paste0("https://www.googleapis.com/bigquery/v2/", rurl)
  
  access_token <- get_cached_token("bigquery", proxy_usage)
  
  x <- GET(rurl, add_headers(Authorization = paste("Bearer", access_token)))
  x <- fromJSON(content(x, "text"))
  x
}